package test;

import org.glassfish.jersey.client.ClientConfig;

import com.crud_java_restful.ws.Person;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class testProgram {
	private static String baseURL = "http://localhost:8080/crud_java_restful/rest/people";
	
	public static void main(String[] args) {
		//testGet();
		//testAdd();
		//testUpdate();
		testDelete();
		testList();
	}
	
	static void testAdd() {
		WebTarget target = getWebTarget();
		Person p = new Person("Fire", 1432, "2021-06-09", "woman");
		Response response = target.request().post(Entity.entity(p, MediaType.APPLICATION_JSON), Response.class);
		System.out.println(response);
	}
	
	static void testGet() {
		WebTarget target = getWebTarget();
		String personId = "1";
		Person p = target.path(personId).request().accept(MediaType.APPLICATION_JSON).get(Person.class);
		
		System.out.println(p);
	}
	
	static void testList() {
		WebTarget target = getWebTarget();
String response = target.request().accept(MediaType.APPLICATION_JSON).get(String.class);
		
		System.out.println(response);
	}
	
	static void testDelete() {
		WebTarget target = getWebTarget();
		String personId = "3";
		Response response = target.path(personId).request().delete(Response.class);
		
		System.out.println(response);
	}
	
	static void testUpdate() {
		WebTarget target = getWebTarget();
		String personId = "3";
		Person p = new Person("Fiona", 1432, "2021-06-09", "woman");
		Response response = target.path(personId).request().put(Entity.entity(p, MediaType.APPLICATION_JSON), Response.class);
		
		System.out.println(response);
	}
	
	static WebTarget getWebTarget() {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		
		return client.target(baseURL);
	}
}
