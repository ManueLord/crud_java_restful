package com.crud_java_restful.ws;

import java.net.*;
import java.util.*;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/people")
public class peopleResource {
	private personDAO dao = personDAO.getInstance();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> list(){
		return dao.listAll();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response add(Person p) throws URISyntaxException {
		int newPersonId = dao.add(p);
		URI uri = new URI("/crud_java_restful/people/" + newPersonId);
		return Response.created(uri).build();
	}
	
	@GET
	@Path("{id}")
	public Response get(@PathParam("id") int id) {
		Person p = dao.get(id);
		if(p != null) 
			return Response.ok(p, MediaType.APPLICATION_JSON).build();
		else
			return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") int id, Person p) {
		p.setId(id);
		if(dao.update(p))
			return Response.ok().build();
		else
			return Response.notModified().build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		if (dao.delete(id))
			return Response.ok().build();
		else
			return Response.notModified().build();
	}
}
