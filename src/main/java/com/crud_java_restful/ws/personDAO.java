package com.crud_java_restful.ws;

import java.util.ArrayList;
import java.util.List;

public class personDAO implements CRUD {

	private static personDAO instance;
	private static List<Person> data = new ArrayList<>();
	
	static {
		data.add(new Person(1, "Mago", 1994, "2021-09-01", "man"));
		data.add(new Person(2, "Miguel", 1665, "2021-08-07", "man"));
	}
	
	private personDAO() {}
	
	public static personDAO getInstance() {
		if (instance == null) {
			instance = new personDAO();
		}
		return instance;
	}
 		
	@Override
	public List<Person> listAll() {
		return new ArrayList<Person>(data);
	}

	@Override
	public int add(Person p) {
		int newId = data.size() + 1;
		p.setId(newId);
		data.add(p);
		return newId;
	}

	@Override
	public Person get(int id) {
		Person  pFind = new Person(id);
		int index = data.indexOf(pFind);
		if (index >= 0)
			return data.get(index);
		return null;
	}

	@Override
	public boolean update(Person p) {
		int index = data.indexOf(p);
		if (index >= 0) {
			data.set(index, p);
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		Person pFind = new Person(id);
		int index = data.indexOf(pFind);
		if (index >= 0) {
			data.remove(index);
			return true;
		}
		return false;
	}

}
