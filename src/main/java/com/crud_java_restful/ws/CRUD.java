package com.crud_java_restful.ws;

import java.util.List;

public interface CRUD {
	public List<Person> listAll();
	public int add (Person p);
	public Person get(int id);
	public boolean update(Person p);
	public boolean delete(int id);
}
