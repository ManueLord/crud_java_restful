package com.crud_java_restful.ws;

public class Person {
	private int id;
	private String name;
	private int year;
	private String date;
	private String gener;
	
	public Person() {
		// TODO Auto-generated constructor stub
	}
	
	public Person(int id) {
		this.id = id;
	}

	public Person(String name, int year, String date, String gener) {
		super();
		this.name = name;
		this.year = year;
		this.date = date;
		this.gener = gener;
	}

	public Person(int id, String name, int year, String date, String gener) {
		this.id = id;
		this.name = name;
		this.year = year;
		this.date = date;
		this.gener = gener;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGener() {
		return gener;
	}

	public void setGener(String gener) {
		this.gener = gener;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", year=" + year + ", date=" + date + ", gener=" + gener + "]";
	}
}
